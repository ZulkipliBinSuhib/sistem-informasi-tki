<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/profile', function() {
    return view('guru-profile');
});
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::resource('/tabel-guru', App\Http\Controllers\Tabel_guruController::class);
Route::resource('/tabel-siswa', App\Http\Controllers\Tabel_siswaController::class);
Route::resource('/barang-rpl', App\Http\Controllers\Tabel_barang_rplController::class);
Route::resource('/barang-tkj', App\Http\Controllers\Tabel_barang_tkjController::class);
