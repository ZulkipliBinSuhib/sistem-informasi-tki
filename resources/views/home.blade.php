@extends('layouts.app')

@section('content')
<div class="row d-flex">
    <!-- Left side columns -->
    <div class="col-lg-12">
          <div class="row">

            <!-- Revenue Card -->
            <div class="col-xxl-3 col-md-8">
              <div class="card info-card revenue-card">
                <div class="card-body">
                  <h5 class="card-title">Jumlah Guru <span></h5>
                  <div class="d-flex align-items-center">
                    <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                      <i class="bi bi-people"></i>
                    </div>
                    <div class="ps-3">
                      <h6>0 Orang</h6>
                      <span class="text-muted small pt-2 ps-1 fw-bold">More info</span>
                    </div>
                  </div>
                </div>
              </div>
            </div><!-- End Revenue Card -->

            <!-- Revenue Card -->
            <div class="col-xxl-3 col-md-8">
              <div class="card info-card customers-card">
                <div class="card-body">
                  <h5 class="card-title">Jumlah Siswa<span></h5>
                  <div class="d-flex align-items-center">
                    <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                      <i class="bi bi-people"></i>
                    </div>
                    <div class="ps-3">
                      <h6>0 Orang</h6>
                      <span class="text-muted small pt-2 ps-1 fw-bold">More info</span>
                    </div>
                  </div>
                </div>
              </div>
            </div><!-- End Revenue Card -->
            
            <!-- Revenue Card -->
            <div class="col-xxl-3 col-md-8">
              <div class="card info-card sales-card">
                <div class="card-body">
                  <h5 class="card-title">Jumlah Barang RPL<span></h5>
                  <div class="d-flex align-items-center">
                    <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                      <i class="bi bi-laptop"></i>
                    </div>
                    <div class="ps-3">
                      <h6>0 Barang</h6>
                      <span class="text-muted small pt-2 ps-1 fw-bold">More info</span>
                    </div>
                  </div>
                </div>
              </div>
            </div><!-- End Revenue Card -->
            
            <!-- Revenue Card -->
            <div class="col-xxl-3 col-md-8">
              <div class="card info-card sales-card">
                <div class="card-body">
                  <h5 class="card-title">Jumlah Barang TKJ<span></h5>
                  <div class="d-flex align-items-center">
                    <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                      <i class="ri-computer-line"></i>
                    </div>
                    <div class="ps-3">
                      <h6>0 Barang</h6>
                      <span class="text-muted small pt-2 ps-1 fw-bold">More info</span>
                    </div>
                  </div>
                </div>
              </div>
            </div><!-- End Revenue Card -->

          </div>
        </div><!-- End Left side columns -->
</div>
        
@endsection
