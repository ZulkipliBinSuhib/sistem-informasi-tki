@extends('layouts.app')

@section('content')
<div class="mx-auto col-md-11 p-4 bg-white text-dark shadow-lg">
    <h2 class="fw-bold text-center mb-5">Tabel Siswa</h2>
    @if(session()->has('status'))
        <div class="alert alert-success" role="alert">{{ session('status') }}</div>
    @endif
    <!-- Basic Modal -->
    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#basicModal">Tambah Data</button>
    <div class="modal fade" id="basicModal" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="/tabel-siswa" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah data</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="mb-3">
                            <label for="nis" class="form-label">Nis</label>
                            <input type="number" class="form-control" id="nis" required name="nis">
                        </div>
                        <div class="mb-3">
                            <label for="nama" class="form-label">Nama</label>
                            <input type="text" class="form-control" id="nama" required name="nama">
                        </div>
                        <div class="mb-3">
                            <label for="agama" class="form-label">Agama</label>
                            <input type="text" class="form-control" id="agama" required name="agama">
                        </div>
                        <div class="mb-3">
                            <label for="jenis_kelamin" class="form-label">Jenis Kelamin</label>
                            <select class="form-select" aria-label="Default select example" name="jenis_kelamin" id="jenis_kelamin">
                                <option value="Laki-Laki">Laki-Laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="tempat_lahir" class="form-label">Tempat Lahir</label>
                            <input type="text" class="form-control" id="tempat_lahir" required name="tempat_lahir">
                        </div>
                        <div class="mb-3">
                            <label for="tanggal_lahir" class="form-label">Tanggal Lahir</label>
                            <input type="date" class="form-control" id="tanggal_lahir" required name="tanggal_lahir">
                        </div>
                        <div class="mb-3">
                            <label for="alamat" class="form-label">Alamat</label>
                            <input type="text" class="form-control" id="alamat" required name="alamat">
                        </div>
                        <div class="mb-3">
                            <label for="image" class="form-label">Gambar</label>
                            <input type="file" class="form-control" id="image" required name="image">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- End Basic Modal-->
        
    <a href="#" class="btn btn-success ms-5">Export pdf</a>
    <form action="" class="float-end">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="nis guru">
            <button class="input-group-text"><i class="bi bi-search"></i></button>
        </div>
    </form>
    <table class="table table-striped mt-5 ">
        <tr>
            <td>No</td>
            <td>Nis</td>
            <td>Nama</td>
            <td>Agama</td>
            <td>Jenis kelamin</td>
            <td>Tempat lahir</td>
            <td>Tanggal Lahir</td>
            <td>Alamat</td>
            <td>Gambar</td>
            <td>Aksi</td>
        </tr>
        @foreach($datas as $data)
        <tr>
            <td>{{ $no++ }}</td>
            <td>{{ $data->nis }}</td>
            <td>{{ $data->nama }}</td>
            <td>{{ $data->agama }}</td>
            <td>{{ $data->jenis_kelamin }}</td>
            <td>{{ $data->tempat_lahir }}</td>
            <td>{{ $data->tanggal_lahir }}</td>
            <td>{{ $data->alamat }}</td>
            <td><img src="{{ asset('storage/' . $data->image) }}" width="80"></td>
            <td>
                 <!-- Basic Modal -->
                <button type="button" class="btn btn-success tombolEdit" data-bs-toggle="modal" data-bs-target="#updatemodal" data-id="{{ $data->id }}">Update</button>
                <div class="modal fade" id="updatemodal" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form action="/tabel-siswa/{{ $data->id }}" method="post"  enctype="multipart/form-data">
                                @csrf
                                @method('put')
                                <div class="modal-header">
                                    <h5 class="modal-title">Update data</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <input type="hidden" id="idedit" required name="id">
                                    <div class="mb-3">
                                        <label for="nis" class="form-label">Nis</label>
                                        <input type="number" class="form-control" id="nisedit" required name="nis">
                                    </div>
                                    <div class="mb-3">
                                        <label for="nama" class="form-label">Nama</label>
                                        <input type="text" class="form-control" id="namaedit" required name="nama">
                                    </div>
                                    <div class="mb-3">
                                        <label for="agama" class="form-label">Agama</label>
                                        <input type="text" class="form-control" id="agamaedit" required name="agama">
                                    </div>
                                    <div class="mb-3">
                                        <label for="agama" class="form-label">Jenis Kelamin</label>
                                        <select class="form-select" aria-label="Default select example" name="jenis_kelamin" id="jenis_kelaminedit">
                                            <option value="Laki-Laki">Laki-Laki</option>
                                            <option value="Perempuan">Perempuan</option>
                                        </select>
                                    </div>
                                    <div class="mb-3">
                                        <label for="tempat_lahir" class="form-label">Tempat Lahir</label>
                                        <input type="text" class="form-control" id="tempat_lahiredit" required name="tempat_lahir">
                                    </div>
                                    <div class="mb-3">
                                        <label for="tanggal_lahir" class="form-label">Tanggal Lahir</label>
                                        <input type="date" class="form-control" id="tanggal_lahiredit" required name="tanggal_lahir">
                                    </div>
                                    <div class="mb-3">
                                        <label for="alamat" class="form-label">Alamat</label>
                                        <input type="text" class="form-control" id="alamatedit" required name="alamat">
                                    </div>   
                                    <div class="mb-3">
                                        <label for="image" class="form-label">Gambar</label>
                                        <input type="file" class="form-control" id="imageedit" name="image">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Edit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- End Basic Modal-->
                <form action="/tabel-siswa/{{$data->id}}" method="post" class="d-inline">
                    @csrf
                    @method('delete')
                    <button type="submit" class="btn btn-danger">Hapus</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
    <div style="padding-top: 20px;">{{ $datas->links() }}</div>
</div>

</div>

<script src="js/jquery.js"></script>
<script>
    $('.tombolEdit').on('click', function() {
    const id = $(this).data('id')
    $.ajax({
        url : `http://127.0.0.1:8000/tabel-siswa/${id}/edit`,
        type : 'get',
        dataType : 'json',
        success : function(data) {
            console.log(data)
            $('#idedit').val(id)
            $('#nisedit').val(data.nis)
            $('#namaedit').val(data.nama)
            $('#tanggal_lahiredit').val(data.tanggal_lahir)
            $('#agamaedit').val(data.agama)
            $('#jenis_kelaminedit').val(data.jenis_kelamin)
            $('#tempat_lahiredit').val(data.tempat_lahir)
            $('#alamatedit').val(data.alamat)
            $('#keteranganedit').val(data.keterangan)
        },
        error: function() {
            console.log('dad')
        }
    })
})
</script>
@endsection