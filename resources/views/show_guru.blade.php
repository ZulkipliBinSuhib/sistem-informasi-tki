@extends('layouts.app')

@section('content')
<section class="section profile">
			<div class="row">
				<div class="col-xl-4">
					<div class="card">
						<div class="card-body profile-card pt-4 d-flex flex-column align-items-center">
							<img src="{{ asset('storage/' . $result->image) }}" alt="Profile" class="rounded-circle" />
							<h2>{{ $result->nama }}</h2>
							<h3>{{ $result->bidang_studi }}</h3>
							<div class="social-links mt-2">
								<a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
								<a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
								<a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
								<a href="#" class="linkedin"><i class="bi bi-linkedin"></i></a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-xl-8">
					<div class="card">
						<div class="card-body pt-3">
							<!-- Bordered Tabs -->
							<ul class="nav nav-tabs nav-tabs-bordered">
								<li class="nav-item">
									<button class="nav-link active" data-bs-toggle="tab"
										data-bs-target="#profile-overview">
										Overview
									</button>
								</li>

								<li class="nav-item">
									<button class="nav-link" data-bs-toggle="tab"
										data-bs-target="#profile-change-password">
										Change Password
									</button>
								</li>
							</ul>
							<div class="tab-content pt-2">
								<div class="tab-pane fade show active profile-overview" id="profile-overview">
									<h5 class="card-title">Keterangan</h5>
									<p class="small fst-italic">
										{{ $result->keterangan }}
									</p>
                                    
									<h5 class="card-title">Profile Details</h5>
                                    
									<div class="row">
                                        <div class="col-lg-3 col-md-4 label">Nama</div>
										<div class="col-lg-9 col-md-8">{{ $result->nama }}</div>
									</div>
                                    
                                    <div class="row">
                                        <div class="col-lg-3 col-md-4 label">Tempat Lahir</div>
                                        <div class="col-lg-9 col-md-8">
                                            {{ $result->tempat_lahir }}
                                        </div>
                                    </div>

                                    <div class="row">
										<div class="col-lg-3 col-md-4 label">Tanggal Lahir</div>
										<div class="col-lg-9 col-md-8">{{ $result->tanggal_lahir }}</div>
									</div>

									<div class="row">
										<div class="col-lg-3 col-md-4 label">Nip</div>
										<div class="col-lg-9 col-md-8">
											{{ $result->nip }}
										</div>
									</div>

                                    <div class="row">
										<div class="col-lg-3 col-md-4 label">Golongan</div>
										<div class="col-lg-9 col-md-8">
											{{ $result->golongan }}
										</div>
									</div>

									<div class="row">
										<div class="col-lg-3 col-md-4 label">Pendidikan</div>
										<div class="col-lg-9 col-md-8">{{ $result->pendidikan }}</div>
									</div>

									<div class="row">
										<div class="col-lg-3 col-md-4 label">Bidang Studi</div>
										<div class="col-lg-9 col-md-8">{{ $result->bidang_studi }}</div>
									</div>
								</div>

								<div class="tab-pane fade profile-edit pt-3" id="profile-edit">
									<!-- Profile Edit Form -->
									<form action="/tabel-guru/{{ $result->id }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        @method('put')
                                        <input type="hidden" name="id" value="{{ $result->id }}">
										<div class="row mb-3">
											<label for="profileImage" class="col-md-4 col-lg-3 col-form-label">Gambar</label>
											<div class="col-md-8 col-lg-9">
												<img src="{{ asset('storage/' . $result->image) }}" width="100" alt="Profile" />
												<div class="pt-2">
													<input type="file" class="form-control" name="image">
												</div>
											</div>
										</div>

										<div class="row mb-3">
											<label for="nama" class="col-md-4 col-lg-3 col-form-label">Nama</label>
											<div class="col-md-8 col-lg-9">
												<input name="nama" type="text" class="form-control" id="nama"
													value="{{ $result->nama }}" />
											</div>
										</div>

										<div class="row mb-3">
											<label for="tempat_lahir"
												class="col-md-4 col-lg-3 col-form-label">Tempat Lahir</label>
											<div class="col-md-8 col-lg-9">
												<input name="tempat_lahir" type="text" class="form-control" id="tempat_lahir"
													value="{{ $result->tempat_lahir }}" />
											</div>
										</div>

										<div class="row mb-3">
											<label for="tanggal_lahir" class="col-md-4 col-lg-3 col-form-label">Tanggal Lahir</label>
											<div class="col-md-8 col-lg-9">
												<input name="tanggal_lahir" type="date" class="form-control" id="tanggal_lahir"
													value="{{ $result->tanggal_lahir }}" />
											</div>
										</div>

										<div class="row mb-3">
											<label for="nip"
												class="col-md-4 col-lg-3 col-form-label">Nip</label>
											<div class="col-md-8 col-lg-9">
												<input name="nip" type="text" class="form-control" id="nip"
													value="{{ $result->nip }}" />
											</div>
										</div>

										<div class="row mb-3">
											<label for="golongan"
												class="col-md-4 col-lg-3 col-form-label">Golongan</label>
											<div class="col-md-8 col-lg-9">
												<input name="golongan" type="text" class="form-control" id="golongan"
													value="{{ $result->golongan }}" />
											</div>
										</div>

										<div class="row mb-3">
											<label for="pendidikan" class="col-md-4 col-lg-3 col-form-label">Pendidikan</label>
											<div class="col-md-8 col-lg-9">
												<input name="pendidikan" type="text" class="form-control" id="pendidikan"
													value="{{ $result->pendidikan }}" />
											</div>
										</div>

                                        <div class="row mb-3">
											<label for="keterangan" class="col-md-4 col-lg-3 col-form-label">Keterangan</label>
											<div class="col-md-8 col-lg-9">
												<textarea name="keterangan" class="form-control" id="keterangan"
													style="height: 100px">{{ $result->keterangan }}</textarea>
                                            </div>
										</div>

										<div class="row mb-3">
											<label for="bidang_studi" class="col-md-4 col-lg-3 col-form-label">Bidang Studi</label>
											<div class="col-md-8 col-lg-9">
												<input name="bidang_studi" type="text" class="form-control" id="bidang_studi"
													value="{{ $result->bidang_studi }}" />
											</div>
										</div>

										<div class="text-center">
											<button type="submit" class="btn btn-primary">Edit</button>
										</div>
									</form>
									<!-- End Profile Edit Form -->
								</div>
							</div>
							<!-- End Bordered Tabs -->
						</div>
					</div>
				</div>
			</div>
		</section>
@endsection