








@extends('layouts.app')

@section('content')
<div class="mx-auto col-md-11 p-4 bg-white text-dark shadow-lg">
    <h2 class="fw-bold text-center mb-5">Tabel Barang TKJ</h2>
    @if(session()->has('status'))
        <div class="alert alert-success" role="alert">{{ session('status') }}</div>
    @endif
    <!-- Basic Modal -->
    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#basicModal">Tambah Data</button>
    <div class="modal fade" id="basicModal" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="/barang-tkj" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah data</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="mb-3">
                            <label for="nama_barang" class="form-label">Nama barang</label>
                            <input type="text" class="form-control" id="nama_barang" required name="nama_barang">
                        </div>
                        <div class="mb-3">
                            <label for="type" class="form-label">Type</label>
                            <input type="text" class="form-control" id="type" required name="type">
                        </div>
                        <div class="mb-3">
                            <label for="jumlah_barang" class="form-label">Jumlah barang</label>
                            <input type="number" class="form-control" id="jumlah_barang" required name="jumlah_barang">
                        </div>
                        <div class="mb-3">
                            <label for="tahun_barang" class="form-label">Tahun barang</label>
                            <input type="number" class="form-control" id="tahun_barang" required name="tahun_barang">
                        </div>
                        <div class="mb-3">
                            <label for="kondisi" class="form-label">Kondisi</label>
                            <select class="form-select" aria-label="Default select example" name="kondisi" id="kondisi" required>
                                <option value="Baik">Baik</option>
                                <option value="Kurang baik">Kurang baik</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="tempat_barang" class="form-label">Tempat barang</label>
                            <input type="text" class="form-control" id="tempat_barang" required name="tempat_barang">
                        </div>
                        <div class="mb-3">
                            <label for="type" class="form-label">Jurusan barang</label>
                            <select required class="form-select" aria-label="Default select example" name="jurusan_barang" id="jurusan_barang">
                                <option value="Rpl">Rpl</option>
                                <option value="Tkj">Tkj</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="image" class="form-label">Gambar</label>
                            <input type="file" class="form-control" id="image" required name="image">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- End Basic Modal-->
        
    <a href="#" class="btn btn-success ms-5">Export pdf</a>
    <form action="" class="float-end">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="nis guru">
            <button class="input-group-text"><i class="bi bi-search"></i></button>
        </div>
    </form>
    <table class="table table-striped mt-5 ">
        <tr>
            <td>No</td>
            <td>Nama Barang</td>
            <td>Type</td>
            <td>Jumlah Barang</td>
            <td>Tahun Perolehan</td>
            <td>Kondisi</td>
            <td>Tempat Barang</td>
            <td>Gambar</td>
            <td>Qr Code</td>
            <td>Aksi</td>
        </tr>
        @foreach($datas as $data)
        <tr>
            <td>{{ $no++ }}</td>
            <td>{{ $data->nama_barang }}</td>
            <td>{{ $data->type }}</td>
            <td>{{ $data->jumlah_barang }}</td>
            <td>{{ $data->tahun_barang }}</td>
            <td>{{ $data->kondisi }}</td>
            <td>{{ $data->tempat_barang }}</td>
            <td><img src="{{ asset('storage/' . $data->image) }}" width="80"></td>
            <td><img src="{{ asset('storage/' . $data->image) }}" width="80"></td>
            <td>
                 <!-- Basic Modal -->
                <button type="button" class="btn btn-success tombolEdit" data-bs-toggle="modal" data-bs-target="#updatemodal" data-id="{{ $data->id }}">Update</button>
                <div class="modal fade" id="updatemodal" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form action="/barang-tkj/{{ $data->id }}" method="post"  enctype="multipart/form-data">
                                @csrf
                                @method('put')
                                <div class="modal-header">
                                    <h5 class="modal-title">Update data</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <input type="hidden" id="idedit" required name="id">
                                    <div class="mb-3">
                                        <label for="nama_barang" class="form-label">Nama_barang</label>
                                        <input type="text" class="form-control" id="nama_barangedit" required name="nama_barang">
                                    </div>
                                    <div class="mb-3">
                                        <label for="type" class="form-label">type</label>
                                        <input type="text" class="form-control" id="typeedit" required name="type">
                                    </div>
                                    <div class="mb-3">
                                        <label for="jumlah_barang" class="form-label">Jumlah barang</label>
                                        <input type="number" class="form-control" id="jumlah_barangedit" required name="jumlah_barang">
                                    </div>
                                    <div class="mb-3">
                                        <label for="tahun_barang" class="form-label">Tahun barang</label>
                                        <input type="number" class="form-control" id="tahun_barangedit" required name="tahun_barang">
                                    </div>
                                    <div class="mb-3">
                                        <label for="type" class="form-label">Kondisi</label>
                                        <select required class="form-select" aria-label="Default select example" name="kondisi" id="kondisiedit">
                                            <option value="Baik">Baik</option>
                                            <option value="Kurang baik">Kurang baik</option>
                                        </select>
                                    </div>
                                    <div class="mb-3">
                                        <label for="tempat_barang" class="form-label">Tempat barang</label>
                                        <input type="text" class="form-control" id="tempat_barangedit" required name="tempat_barang">
                                    </div>   
                                    <div class="mb-3">
                                        <label for="type" class="form-label">Jurusan barang</label>
                                        <select required class="form-select" aria-label="Default select example" name="jurusan_barang" id="jurusan_barangedit">
                                            <option value="Rpl">Rpl</option>
                                            <option value="Tkj">Tkj</option>
                                        </select>
                                    </div>
                                    <div class="mb-3">
                                        <label for="image" class="form-label">Gambar</label>
                                        <input type="file" class="form-control" id="imageedit" name="image">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Edit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- End Basic Modal-->
                <form action="/barang-tkj/{{$data->id}}" method="post" class="d-inline">
                    @csrf
                    @method('delete')
                    <button type="submit" class="btn btn-danger">Hapus</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
    <div style="padding-top: 20px;">{{ $datas->links() }}</div>
</div>

</div>

<script src="js/jquery.js"></script>
<script>
    $('.tombolEdit').on('click', function() {
    const id = $(this).data('id')
    $.ajax({
        url : `http://127.0.0.1:8000/barang-tkj/${id}/edit`,
        type : 'get',
        dataType : 'json',
        success : function(data) {
            console.log(data)
            $('#idedit').val(id)
            $('#nama_barangedit').val(data.nama_barang)
            $('#tahun_barangedit').val(data.tahun_barang)
            $('#typeedit').val(data.type)
            $('#kondisiedit').val(data.kondisi)
            $('#jumlah_barangedit').val(data.jumlah_barang)
            $('#tempat_barangedit').val(data.tempat_barang)
            $('#jurusan_barangedit').val(data.jurusan_barang)
        },
        error: function() {
            console.log('error')
        }
    })
})
</script>
@endsection