@extends('layouts.app')

@section('content')
<div class="mx-auto col-md-11 p-4 bg-white text-dark shadow-lg">
    <h2 class="fw-bold text-center mb-5">Nama Guru Produktif TKI</h2>
    @if(session()->has('status'))
        <div class="alert alert-success" role="alert">{{ session('status') }}</div>
    @endif
    <!-- Basic Modal -->
    <div class="modal fade" id="basicModal" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form action="/tabel-guru" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah data</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="mb-3">
                            <label for="nama" class="form-label">Nama</label>
                            <input type="text" class="form-control" id="nama" required name="nama">
                        </div>
                        <div class="mb-3">
                            <label for="tempat_lahir" class="form-label">Tempat Lahir</label>
                            <input type="text" class="form-control" id="tempat_lahir" required name="tempat_lahir">
                        </div>
                        <div class="mb-3">
                            <label for="tanggal_lahir" class="form-label">Tanggal Lahir</label>
                            <input type="date" class="form-control" id="tanggal_lahir" required name="tanggal_lahir">
                        </div>
                        <div class="mb-3">
                            <label for="nip" class="form-label">Nip</label>
                            <input type="number" class="form-control" id="nip" required name="nip">
                        </div>
                        <div class="mb-3">
                            <label for="golongan" class="form-label">Golongan</label>
                            <input type="text" class="form-control" id="golongan" required name="golongan">
                        </div>
                        <div class="mb-3">
                            <label for="pendidikan" class="form-label">Pendidikan</label>
                            <input type="text" class="form-control" id="pendidikan" required name="pendidikan">
                        </div>
                        <div class="mb-3">
                            <label for="bidang_studi" class="form-label">Bidang studi</label>
                            <input type="text" class="form-control" id="bidang_studi" required name="bidang_studi">
                        </div>
                        <div class="mb-3">
                            <label for="keterangan" class="form-label">Keterangan</label>
                            <input type="text" class="form-control" id="keterangan" required name="keterangan">
                        </div>
                        <div class="mb-3">
                            <label for="image" class="form-label">Image</label>
                            <input type="file" class="form-control" id="image" required name="image">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- End Basic Modal-->
    <div class="row">

    </div>
    <button type="button" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#basicModal">Tambah Data</button>    
    <a href="#" class="btn btn-success ms-5 btn-sm">Export pdf</a>
    <form action="" class="float-end">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Nama guru">
            <button class="input-group-text"><i class="bi bi-search"></i></button>
        </div>
    </form>
    <div class=" table table-responsive">
        <table class="table table-striped mt-5 ">
            <tr>
                <th>No</th>
                <th>Nama Lengkap</th>
                <!-- <th>Tempat Lahir</th>
                <th>Tanggal Lahir</td> -->
                <th>Nip</th>
                <th>Golongan</th>
                <!-- <th>Pendidikan</th> -->
                <th>Keterangan</th>
                <th>Bidang Studi</th>
                <!-- <th>Gambar</th> -->
                <th>Aksi</th>
            </tr>
            @foreach($datas as $data)
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $data->nama }}</td>
                <!-- <td>{{ $data->tempat_lahir }}</td>
                <td>{{ $data->tanggal_lahir }}</td> -->
                <td>{{ $data->nip }}</td>
                <td>{{ $data->golongan }}</td>
                <!-- <td>{{ $data->pendidikan }}</td> -->
                <td>{{ $data->keterangan }}</td>
                <td>{{ $data->bidang_studi }}</td>
                <!-- <td><img src="{{ asset('storage/' . $data->image) }}" width="80"></td> -->
                <td>
                
                    <a href="{{url('tabel-guru/'.$data->id)}}" title="Profile" class="btn btn-sm btn-info"><i class="bi bi-person-square"></i></a>
                    <a href="{{url('tabel-guru/'.$data->id)}}" title="Barcode" class="btn btn-sm btn-secondary"><i class="bi bi-upc-scan"></i></a>
                     <!-- Basic Modal -->
                    <!-- <button type="button" class="btn btn-success tombolEdit btn-sm" title="Ubah Data" data-bs-toggle="modal" data-bs-target="#updatemodal" data-id="{{ $data->id }}"><i class="bi bi-pencil-square"></i></button> -->
                    <!-- <button type="button" class="btn btn-success tombolEdit btn-sm" title="Ubah Data" data-bs-toggle="modal" data-bs-target="#updatemodal" data-id="{{ $data->id }}"><i class="bi bi-pencil-square"></i></button> -->
                    <!-- <div class="modal fade" id="updatemodal" tabindex="-1">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <form action="/tabel-guru/{{ $data->id }}" method="post"  enctype="multipart/form-data">
                                    @csrf
                                    @method('put')
                                    <div class="modal-header">
                                        <h5 class="modal-title">Update data</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <input type="hidden" id="idedit" required name="id">
                                        <div class="mb-3">
                                            <label for="nama" class="form-label">Nama</label>
                                            <input type="text" class="form-control" id="namaedit" required name="nama">
                                        </div>
                                        <div class="mb-3">
                                            <label for="tempat_lahir" class="form-label">Tempat Lahir</label>
                                            <input type="text" class="form-control" id="tempat_lahiredit" required name="tempat_lahir">
                                        </div>
                                        <div class="mb-3">
                                            <label for="tanggal_lahir" class="form-label">Tanggal Lahir</label>
                                            <input type="date" class="form-control" id="tanggal_lahiredit" required name="tanggal_lahir">
                                        </div>
                                        <div class="mb-3">
                                            <label for="nip" class="form-label">Nip</label>
                                            <input type="number" class="form-control" id="nipedit" required name="nip">
                                        </div>
                                        <div class="mb-3">
                                            <label for="golongan" class="form-label">Golongan</label>
                                            <input type="text" class="form-control" id="golonganedit" required name="golongan">
                                        </div>
                                        <div class="mb-3">
                                            <label for="pendidikan" class="form-label">Pendidikan</label>
                                            <input type="text" class="form-control" id="pendidikanedit" required name="pendidikan">
                                        </div>
                                        <div class="mb-3">
                                            <label for="bidang_studi" class="form-label">Bidang studi</label>
                                            <input type="text" class="form-control" id="bidang_studiedit" required name="bidang_studi">
                                        </div>
                                        <div class="mb-3">
                                            <label for="keterangan" class="form-label">Keterangan</label>
                                            <input type="text" class="form-control" id="keteranganedit" required name="keterangan">
                                        </div>
                                        <div class="mb-3">
                                            <label for="image" class="form-label">Image</label>
                                            <input type="file" class="form-control" id="imageedit" name="image">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" style="float:left;">Batal</button>
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div> -->
                    <!-- End Basic Modal-->
                    <!-- <form action="/tabel-guru/{{$data->id}}" method="post" class="d-inline">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger btn-sm" title="hapus Data"><i class="bi bi-trash"></i></button>
                    </form> -->
                </td>
            </tr>
            @endforeach
        </table>
        
        <div style="padding-top: 20px;">{{ $datas->links() }}</div>
    </div>
</div>

</div>

<script src="js/jquery.js"></script>
<script>
    $('.tombolEdit').on('click', function() {
    const id = $(this).data('id')
    $.ajax({
        url : `{{url('tabel-guru/${id}/edit')}}`,
        type : 'get',
        dataType : 'json',
        success : function(data) {
            $('#idedit').val(id)
            $('#namaedit').val(data.nama)
            $('#tempat_lahiredit').val(data.tempat_lahir)
            $('#tanggal_lahiredit').val(data.tanggal_lahir)
            $('#nipedit').val(data.nip)
            $('#golonganedit').val(data.golongan)
            $('#pendidikanedit').val(data.pendidikan)
            $('#bidang_studiedit').val(data.bidang_studi)
            $('#keteranganedit').val(data.keterangan)
        },
        error: function() {
            console.log('error')
        }
    })
})
</script>
@endsection