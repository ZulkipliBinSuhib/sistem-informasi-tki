<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->insert([
            'name' => "Admin",
            'email' => "admin@gmail.com",
            'password' => Hash::make('12345678'),
        ]);
        for($i = 1; $i < 100; $i++) {
            
        DB::table('tb_gurus')->insert([
            'nama' => 'a',
            'tempat_lahir' => 'a',
            'tanggal_lahir' => '2022-01-01',
            'nip' => '1',
            'golongan' => 'a',
            'pendidikan' => 'a',
            'keterangan' => 'a',
            'bidang_studi' => 'a',
            'image' => 'a',
        ]);
        
        DB::table('tb_siswas')->insert([
            'nis' => '1',
            'nama' => 'a',
            'agama' => 'islam',
            'jenis_kelamin' => 'Laki-Laki',
            'tempat_lahir' => 'a',
            'tanggal_lahir' => '2022-01-01',
            'alamat' => 'a',
            'image' => 'a',
        ]);

        DB::table('tb_barangs')->insert([
            'nama_barang' => 'a',
            'type' => 'a',
            'jumlah_barang' => '100',
            'tahun_barang' => '2000',
            'kondisi' => 'Baik',
            'tempat_barang' => 'a',
            'jurusan_barang' => 'Tkj',
            'image' => 'a',
        ]);

        DB::table('tb_barangs')->insert([
            'nama_barang' => 'a',
            'type' => 'a',
            'jumlah_barang' => '100',
            'tahun_barang' => '2000',
            'kondisi' => 'Baik',
            'tempat_barang' => 'a',
            'jurusan_barang' => 'Rpl',
            'image' => 'a',
        ]);
    }
        
    }
}
