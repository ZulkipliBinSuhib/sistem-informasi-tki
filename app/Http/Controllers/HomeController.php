<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function tabelGuru() {
        return view('tabel-guru');
    }

    public function tabelSiswa() {
        return view('tabel-siswa');
    }

    public function barangTkj() {
        return view('barang-tkj');
    }

    public function barangRpl() {
        return view('barang-rpl');
    }
}
