<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tb_siswa;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class Tabel_siswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tabel-siswa', [
            'datas' => DB::table('tb_siswas')->paginate(10),
            'no' => 1
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nis' => "required",
            'nama' => "required",
            'agama' => "required",
            'jenis_kelamin' => "required",
            'tempat_lahir' => "required",
            'tanggal_lahir' => "required",
            'alamat' => "required",
            'image' => "required",
        ]);

        if($request->file('image')) {
            $validatedData['image'] = $request->file('image')->store('tb_siswa');
        }
        
        Tb_siswa::create($validatedData);

        return redirect('/tabel-siswa')->with('status', 'Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $datas = Tb_siswa::where('id', $id)->get();
        echo json_encode($datas[0]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $datas = Tb_siswa::where('id', $id)->get();
        $validatedData = $request->validate([
            'nis' => "required",
            'nama' => "required",
            'agama' => "required",
            'jenis_kelamin' => "required",
            'tempat_lahir' => "required",
            'tanggal_lahir' => "required",
            'alamat' => "required",
        ]);
        


        if(!$request->file('image')) {
            $validatedData['image'] = $datas[0]->image;
            // $validatedData['image'] = $datas[0]->image;
        } else {
            Storage::delete($datas[0]->image);
            $validatedData['image'] = $request->file('image')->store('tb_siswa');
        }

        Tb_siswa::where('id', $request->id)->update($validatedData);

        return redirect('/tabel-siswa')->with('status', 'Data berhasil di edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Tb_siswa::destroy($id);
        return redirect('/tabel-siswa')->with('status', 'Data berhasil dihapus');
    }
}
