<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tb_barang;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class Tabel_barang_tkjController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return view('barang-tkj', [
            'datas' => DB::table('tb_barangs')->where('jurusan_barang', 'TKj')->paginate(10),
            'no' => 1
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama_barang' => "required",
            'type' => "required",
            'jumlah_barang' => "required",
            'tahun_barang' => "required",
            'kondisi' => "required",
            'tempat_barang' => "required",
            'jurusan_barang' => "required",
            'image' => "required",
        ]);

        if($request->file('image')) {
            $validatedData['image'] = $request->file('image')->store('tb_barang');
        }
        
        Tb_barang::create($validatedData);

        return redirect('/barang-tkj')->with('status', 'Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $datas = Tb_barang::where('id', $id)->get();
        echo json_encode($datas[0]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $datas = Tb_barang::where('id', $id)->get();
        $validatedData = $request->validate([
            'nama_barang' => "required",
            'type' => "required",
            'jumlah_barang' => "required",
            'tahun_barang' => "required",
            'kondisi' => "required",
            'tempat_barang' => "required",
            "jurusan_barang" => "required"
        ]);
        
        
        
        if(!$request->file('image')) {
            $validatedData['image'] = $datas[0]->image;
            // $validatedData['image'] = $datas[0]->image;
        } else {
            Storage::delete($datas[0]->image);
            $validatedData['image'] = $request->file('image')->store('tb_barang');
        }

        Tb_barang::where('id', $request->id)->update($validatedData);

        return redirect('/barang-tkj')->with('status', 'Data berhasil di edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Tb_barang::destroy($id);
        return redirect('/barang-tkj')->with('status', 'Data berhasil dihapus');
    }
}