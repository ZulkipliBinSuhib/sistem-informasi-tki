<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tb_guru;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class Tabel_guruController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tabel-guru', [
            'datas' => DB::table('tb_gurus')->paginate(10),
            'no' => 1
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => "required",
            'tempat_lahir' => "required",
            'tanggal_lahir' => "required",
            'nip' => "required",
            'golongan' => "required",
            'pendidikan' => "required",
            'keterangan' => "required",
            'bidang_studi' => "required",
            'image' => "required",
        ]);

        if($request->file('image')) {
            $validatedData['image'] = $request->file('image')->store('tb_guru');
        }
        
        Tb_guru::create($validatedData);

        return redirect('/tabel-guru')->with('status', 'Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $data['result'] = Tb_guru::find($id);
       return view('show_guru',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $datas = Tb_guru::where('id', $id)->get();
        echo json_encode($datas[0]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $datas = Tb_guru::where('id', $id)->get();
        $validatedData = $request->validate([
            'nama' => "required",
            'tempat_lahir' => "required",
            'tanggal_lahir' => "required",
            'nip' => "required",
            'golongan' => "required",
            'pendidikan' => "required",
            'keterangan' => "required",
            'bidang_studi' => "required",
        ]);
        

        if(!$request->file('image')) {
            $validatedData['image'] = $datas[0]->image;
            // $validatedData['image'] = $datas[0]->image;
        } else {
            Storage::delete($datas[0]->image);
            $validatedData['image'] = $request->file('image')->store('tb_guru');
        }

        Tb_guru::where('id', $request->id)->update($validatedData);

        return redirect('/tabel-guru')->with('status', 'Data berhasil di edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Tb_guru::destroy($id);
        return redirect('/tabel-guru')->with('status', 'Data berhasil dihapus');
    }
}
